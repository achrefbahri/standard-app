import React from "react";
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import Register from "./pages/register/Register";
import NotFound from "./hocs/notFound/NotFound"




const listRouter = () => [
    { "Path": "/login", "Component": Login, isPublic: true, withHeader: false, withFooter: false },
    { "Path": "/register", "Component": Register, isPublic: true, withHeader: false, withFooter: true },
    { "Path": "/home", "Component": Home, isPublic: false, withHeader: true, withFooter: true },
    { "Path": "/not-found", "Component": NotFound, isPublic: true, withHeader: false, withFooter: true },

]

export default listRouter;
