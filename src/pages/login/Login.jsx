import React, { Component } from 'react';
import FormLogin from "./../../containers/FormLogin/FormLogin";
import CardSignInSignUp from "./../../components/UI/Cards/CardSignInSignUp/CardSignInSignUp"
import PropTypes from 'prop-types';
import "./Login.css"

class Login extends Component {
    constructor(props) {
        super(props);

    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    shouldComponentUpdate(nextProps, nextState) {
        return true
    }

    componentWillUpdate(nextProps, nextState) {

    }

    componentDidUpdate(prevProps, prevState) {

    }

    componentWillUnmount() {

    }

    render() {
        return (
            <div className="container login_page">
                <CardSignInSignUp>
                    <FormLogin />
                </CardSignInSignUp>
            </div>
        );
    }
}

Login.propTypes = {

};

export default Login;