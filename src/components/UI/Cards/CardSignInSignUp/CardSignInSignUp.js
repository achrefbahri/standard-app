import React from "react";
import Button from "../../Button/Button"
import "./CardSignInSignUp.css"

const CardSignInSignUp = ({ children }) => {
    return <div className="CardSignInSignUp">
        <div className="log_signIn_signUp">
            <i className="fa fa-user" />
        </div>
        {children}
        <div className="social_signIn" >
            <Button btnType="facebook" />
            <Button btnType="twitter" />
            <Button btnType="instagram" />

        </div>
    </div>
}

export default CardSignInSignUp;