import React from 'react';
import "./Button.css"

const button = ({ disabled, btnType, clicked, title }) => {
    var ButtonElement;
    switch (btnType) {
        case "facebook": ButtonElement = (
            <button className="btn btn-facebook waves-effect btn-circle waves-light" type="button"> </button>
        );
            break;
        case "instagram": ButtonElement = (
            <button className="btn btn-instagram waves-effect btn-circle waves-light" type="button"></button>
        );
            break;
        case "gmail": ButtonElement = (
            <button className="btn btn-googleplus waves-effect waves-light" type="button"> <i className="fa fa-google-plus"></i> </button>
        );
            break;
        case "youtube": ButtonElement = (
            <button className="btn btn-youtube waves-effect waves-light" type="button"> <i className="fa fa-youtube"></i> </button>
        );
            break;
        case "twitter": ButtonElement = (
            <button class="btn btn-twitter waves-effect btn-circle waves-light" type="button"> </button>
        );
            break;
        default: (ButtonElement = < button
            disabled={disabled}
            className={["Button", btnType].join(' ')}
            onClick={clicked} > {title}</button >)
    }

    return ButtonElement
}


export default button;