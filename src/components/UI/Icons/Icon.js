import React from "react";
import { FaAngleLeft } from "react-icons";



function Icon({ class_name = "" }) {
    const classes = [class_name, "icon"]
    return <FaAngleLeft className={classes.join(" ")} />
}