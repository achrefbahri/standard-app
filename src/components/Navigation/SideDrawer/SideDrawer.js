import React from 'react';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import Backdrop from '../../UI/Backdrop/Backdrop';
import SideMenu from "./../SideMenu/SideMenu"
import './SideDrawer.css';


import { Link } from "react-router-dom"
const sideDrawer = (props) => {

    let attachedClasses = ["SideDrawer", "Close"];
    if (props.open) {
        attachedClasses = ["SideDrawer", "Open"];
    }
    return (
        <>
        <Backdrop show={props.open} clicked={props.closed} />
        <div className={attachedClasses.join(' ')}>
            <div className="Logo_drawer">
                <Logo />
                <NavigationItems />
            </div>


        </div>
        </>
    );
};

export default sideDrawer;