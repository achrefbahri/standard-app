import React, { Component } from 'react';
import { Link } from "react-router-dom";
import MetisMenu from 'metismenujs';
import Logo from "./../../Logo/Logo"
import 'metismenujs/dist/metismenujs.css';
import "./SideMenu.css"


const listSidebar = [
    {
        name: "dashboard",
        path: "/#",
        hasChild: false,
        icon: "fa fa-bar-chart"

    },
    {
        name: "test",
        path: "/#",
        hasChild: true,
        icon: "fa fa-user"
        , childs: [
            { name: "list", path: "/list", icon: "fa fa-list" },
            { name: "update", path: "/update", icon: "fa fa-pencil-square-o" },
            { name: "detail", path: "AffectAnfragen", icon: "fa fa-pencil-square-o" },
        ],
    },
    {
        name: "calendar",
        path: "calendar",
        hasChild: false,
        icon: "fa fa-calendar"
    },
    {
        name: "reservation",
        path: "companyTransport",
        hasChild: false,
        icon: "fa fa-bus"
    },
    {
        name: "users",
        path: "/#",
        hasChild: true,
        icon: "fa fa-user"
        , childs: [
            { name: "list", path: "/list", icon: "fa fa-list" },
            { name: "update", path: "/update", icon: "fa fa-pencil-square-o" },
            { name: "detail", path: "AffectAnfragen", icon: "fa fa-pencil-square-o" },
        ],
    }
]
class MM extends Component {



    componentDidMount() {
        this.$el = this.el;
        this.mm = new MetisMenu(this.$el);
    }
    componentWillUnmount() {
      //  this.mm('dispose');
    }
    render() {
        return (
            <div className="sideBarLeft">
                <div className="logo_sideBarLeft" >
                    <Logo />
                </div>
                <div className="mm-wrapper">
                    <ul className="metismenu" ref={el => this.el = el}>
                        {this.props.children}
                    </ul>
                </div>
            </div>
        );
    }
}

class SideMenu extends Component {

    render() {
        return (
            <MM>

                {
                    listSidebar.map(item => {
                        if (item.hasChild)
                            return <li>
                                <Link className="waves-effect label_item_sideBar " to={item.path} aria-expanded="false">
                                    <>
                                    <i className={item.icon}></i>
                                    <span className="name_item_sideBar">   {item.name}</span>
                                    </>
                                    <i className="fa fa-chevron-right has_arrow_item_sideBar" />
                                </Link>
                                <ul>
                                    {
                                        item.childs.map(child => {
                                            return <li>
                                                <Link to={child.path}>
                                                    <div className="child_item_sideBar">
                                                        <i className={child.icon} />
                                                        <span className="name_item_sideBar">
                                                            {child.name}
                                                        </span>
                                                    </div>
                                                </Link>
                                            </li>

                                        })
                                    }
                                </ul>
                            </li>
                        else
                            return <li>
                                <Link className="waves-effect label_item_sideBar"
                                    to={item.path}
                                    aria-expanded="false">

                                    <i className={item.icon} />
                                    <span className="name_item_sideBar">
                                        {item.name}
                                    </span>
                                </Link>
                            </li >
                    })
                }



                {/* <Link className="waves-effect has-arrow" href="/#"  aria-expanded="false">
                        <span className="fa fa-fw fa-github fa-lg "></span>
                        metisMenu
                     </Link>
                    <ul>
                        <li>
                           <Link to={child.path}>
                                <span className="fa fa-fw fa-code-fork"></span> Fork
                         </Link>
                        </li>
                        <li>
                           <Link href="https://github.com/onokumus/metismenujs">
                                <span className="fa fa-fw fa-star"></span> Star
                         </Link>
                        </li>
                        <li>
                           <Link href="https://github.com/onokumus/metismenujs/issues">
                                <span className="fa fa-fw fa-exclamation-triangle"></span> Issues
                         </Link>
                        </li>
                    </ul> */}


            </MM >
        );
    }
}

export default SideMenu;