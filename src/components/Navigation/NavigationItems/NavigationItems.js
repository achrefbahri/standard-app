import React from 'react';

import NavigationItem from './NavigationItem/NavigationItem';
import './NavigationItems.css';

const navigationList = [
    { path: "/login", label: "Sign in", "icon": "" },
    { path: "/", label: "Home", "icon": "" },
    { path: "/register", label: "Sign up", "icon": "" },
]

const navigationItems = () => (
    <ul className="NavigationItems">
        {
            navigationList.map(({ path, label, icon }) => <NavigationItem
                key={label}
                link={path}
                icon={icon}
            >{label}</NavigationItem>)
        }

    </ul>
);

export default navigationItems;