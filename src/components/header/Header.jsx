import React, { Component } from 'react';
import NavigationItems from "./../../components/Navigation/NavigationItems/NavigationItems";
import SideDrawer from "./../../components/Navigation/SideDrawer/SideDrawer";
import DrawerToggle from "./../../components/Navigation/SideDrawer/DrawerToggle/DrawerToggle";
import Logo from "./../Logo/Logo";

import PropTypes from 'prop-types';
import "./header.css";

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSideDrawer: false,
            open: false
        }

    }


    sideDrawerClosedHandler = () => {
        this.setState({ showSideDrawer: false });
    }

    sideDrawerToggleHandler = () => {
        this.setState((prevState) => {
            return { showSideDrawer: !prevState.showSideDrawer };
        });
    }


    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    shouldComponentUpdate(nextProps, nextState) {
        return true
    }

    componentWillUpdate(nextProps, nextState) {

    }

    componentDidUpdate(prevProps, prevState) {

    }

    componentWillUnmount() {

    }

    render() {
        return (
            <div className="header_container">


                <div className="logo_header">
                    <Logo />
                </div>
                <DrawerToggle clicked={this.sideDrawerToggleHandler} />

                <SideDrawer closed={this.sideDrawerClosedHandler}
                    open={this.state.showSideDrawer}

                />
                <nav>
                    <NavigationItems />
                </nav>
            </div>
        );
    }
}

Header.propTypes = {

};

export default Header;