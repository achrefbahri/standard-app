import React, { Component } from 'react';

import "./footer.css"
import PropTypes from 'prop-types';

class Footer extends Component {
    constructor(props) {
        super(props);

    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    shouldComponentUpdate(nextProps, nextState) {
        return true
    }

    componentWillUpdate(nextProps, nextState) {

    }

    componentDidUpdate(prevProps, prevState) {

    }

    componentWillUnmount() {

    }

    render() {
        return (
            <div className="footer_container shadowInset">
            © 2020 template reactjs

            </div>
        );
    }
}

Footer.propTypes = {

};

export default Footer;