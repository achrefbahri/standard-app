import React from 'react';

import appLogo from '../../assets/images/logo.png';
import  './logo.css';

const logo = (props) => (
    <div className="Logo"  >
        <img src={appLogo} alt="logo" />
    </div>
);

export default logo;