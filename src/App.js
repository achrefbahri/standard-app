import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import Header from "./components/header/Header"
 import Footer from "./components/footer/Footer"
import listRouter from "./listRouter";
import SideMenu from "./components/Navigation/SideMenu/SideMenu"
import './App.css';


const isAuthenticated = false

function ProtectedRoute({ isPublic, path, withHeader, withFooter, component: Component }) {
  if (!isPublic && !isAuthenticated)
    return <Route render={(props) => <Redirect to="/login" />} />
  if (withHeader && withFooter)
    return <Route exact path={path} render={(props) => <><Header />
 
      <div className="container_pages_with_header_footer"><Component {...props} /><Footer /></div>
      </>} />
  if (withHeader)
    return <Route exact path={path} render={(props) => <><Header />
      <div className="container_pages_with_header"><Component {...props} /></div>
      </>} />
  if (withFooter)
    return <Route exact path={path} render={(props) => <div className="container_pages_with_footer">
      <Component {...props} />
      <Footer />
    </div>} />
  return <Route exact path={path} render={(props) => <Component {...props} />} />

}

function App() {
  const listRouters = listRouter()
  return (
    <div className="App">
      <Router>
        <Switch>
          {
            listRouters.map(({ Path, Component, isPublic, withHeader, withFooter }) => <ProtectedRoute
              isPublic={isPublic}
              key={Path}
              path={Path}
              withHeader={withHeader}
              withFooter={withFooter}
              component={Component}
            />)
          }

          <Redirect from="/" exact to="/home" />
          <Redirect to="not-found" />
        </Switch>

      </Router>

    </div>
  );
}

export default App;
